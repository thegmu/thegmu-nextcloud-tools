thegmu/test/git/nextcloud_testdata/README.txt

The files in this repo are to be copied to an empty, simulated, top level
NextCloud file system layout reflecting a GIT repo.

NextCloud stores files in the same directly layout as appears in the UI. There is additional meta information that requires the files be managed via DavFS. Files must be managed via DavFS. The NextCloud server implements a custom DavFS server that includes updating a database of additional file information and so files must be managed via DavFS and cannot be managed by direct file manipulation. 

Test Cases

1. Create an empty NextCloud top level directory. Each test run should destroy and previous work and start over.  For example:

  thegmu/test/data/thegmu_nextcloud_tools/files

2. Sync using the application. Validation will fail because the simulated directory had no files copied. The sync copies via DavFS to any actual server. An error No files will be sync'ed to this directory because the Sync program will mount the actual NextCloud server using DavFS and copy to a server that already has these files. This means this is a negative test. The sync program will look to this simulated directory for validation and nothing will be there because the files where "sync'ed" to the live server.

