#!/bin/bash

SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
TODAY=$(date +"%Y-%m-%d")
STATDATECMD='stat -c "%y" $STATFILE  | cut -d' ' -f 1'
MAILTO="greggy@thegmu.com mybrid@thegmu.com"
#MAILTO="mybrid@thegmu.com"
MAILFROM="greggy@thegmu.com"
MAILDAY=`date +"%m/%d/%Y"`
MAILSUBJECT="[THEGMU] $MAILDAY NextCloud 'gmucorp' files processed yesterday."

GITCLOUDSYNC_SCRIPT="/usr/local/bin/thegmu_nextcloud_git_sync.py"
GITCLOUDSYNC_YAML="/etc/thegmu/thegmu_nextcloud_git_sync.gmucorp.yaml"
GITCLOUDSYNC_MAIL_FILE="/tmp/thegmu_nextcloud_git_sync_mail.txt"

GITCLOUDSYNC_REPORT="$GITCLOUDSYNC_SCRIPT file not found."

if [ -f $GITCLOUDSYNC_SCRIPT ]; then
    echo "$GITCLOUDSYNC_SCRIPT running..."
    python3 $GITCLOUDSYNC_SCRIPT --yesterday_report $GITCLOUDSYNC_YAML > $GITCLOUDSYNC_MAIL_FILE 2>&1
else
    echo $GITCLOUDSYNC_REPORT > $GITCLOUDSYNC_MAIL_FILE
fi

sudo su greggy -c "/usr/bin/mail -r \"$MAILTO\" -s\"$MAILSUBJECT\" $MAILTO <  $GITCLOUDSYNC_MAIL_FILE"
