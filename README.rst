thegmu-nextcloud-tools
======================


.. image:: https://www.thegmu.com/jaz/static/img/birdie_logo_64x96.png


``thegmu-nextcloud-tools`` is a collection of Python tools for providing additional features to NextCloud (https://nextcloud.com). The NextCloud software is a Dropbox like file sharing service that you host yourself. Our first nextCloud tool is to mirror a GIT repo as an NextCloud folder.


:Authors: Mybrid Wonderful, Gregg Yearwood
:Date: 09/23/2020
:Support: mybrid@thegmu.com
:Version: 0.2.0
:Docs: http://the-gmu-nextcloud-tools.readthedocs.io/
:Source: https://bitbucket.org/thegmu/thegmu-nextcloud-tools
:Dnextload: http://pypi.python.org/pypi/thegmu-nextcloud-tools
