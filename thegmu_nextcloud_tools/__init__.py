# -*- coding: utf-8 -*-
"""
The GMU version for every Python project that is compliant to push to PyPi.
"""

__version__ = "0.2.0"
