Introduction
============

.. _intro_dependencies:

System requirements and dependencies
------------------------------------

This is a small scripting application with only one Python library used: PyYAML. However, Linux shell commands are used because different accounts owning different directories requires the use of 'sudo'. 

#. nextCloud 10.1.0 system requirements and dependencies: https://doc.nextcloud.org/server/10.1/admin_manual/installation/system_requirements.html
#. Python 3.6/7 - We strongly recommend a Python virtualenv is used. 
#. PyYAML - Configuration files are in yaml. 
#. cron - cron is used to invoke the mirror script once-per-minute. 
#. davfs - The nextCloud, DavFS URL for the user account is mounted in some `/media` directory as a DavFS mount point for accessing the GIT directory.

.. _intro_installation:

Package Installation
--------------------

This package is available from the `Python Package Index
<http://pypi.python.org/pypi/thegmu-nextcloud-tools>`_. If you have `pip
<https://pip.pypa.io/>`_ you should be able to do::

    $ pip install thegmu-nextcloud-tools

You can also download manually, extract and run ``python setup.py
install``.

Package :mod:`thegmu-nextcloud-tools` has been deployed using nextCloud 10.1.0 and Python 3.6.

.. _intro_planning:

System Administration Planning
------------------------------

Configuration beyond package installatoin will require setting system administration policy and planning thereof.

After installing the Python package what remains is configuration of various server files and services. The package currently requires manual configuration because various policies such as the number of git repos, how frequent to synchronize, and email policy all require planning of wide variability.  The Python package represents the part of the application that requires no planning. After installing the package the policy and planning concerns are with the configuration file. The configuration file mostly addresses the GIT repo directory, the nextCloud directory and then their respective accounts. The final stage of policy and planning has to do with the cron jobs. How often should the synchronization happen? The script supports as synchronization as frequently as every minute. How often should email be sent and then again who is on the mailing list? 

.. note:: The server installation requires running the script with an option to create at least one configuration file. The configuration files are passed to the synchronization script as a command line option within a cron job. The cron job utilizes environment variables intended to be set in the contab file that contain the list configuration file names. 


Cron job script:

::
     
    #!/bin/bash
    
    #Ensure thegmu_nextcloud_git_sync.py is in PATH.
    
    #PATH=$PATH:/usr/local/bin
    
    #If running from a virtualenv then use activate.
    # . /path/to/your/venv/bin/activate
    
    # crontab file:
    # THEGMU_CONFIG_DIR="/etc/thegmu"
    # THEGMU_OCGS_CONFIG_FILES="gitcloudsync.host.yaml gitcloudsync.youraccount.yaml"

    if [ "$THEGMU_CONFIG_DIR" == "" ]; then
        echo "$0: Missing environment variable THEGMU_CONFIG_DIR."
        exit -1
    fi
    
    if [ "$THEGMU_OCGS_CONFIG_FILES" == "" ]; then
        echo "$0: Missing environment variable THEGMU_OCGS_CONFIG_FILES."
        exit -1
    fi
    
    for configfile in $CONFIGFILES; do
        sudo thegmu_nextcloud_git_sync.py $CONFIGDIR/$configfile
    done

