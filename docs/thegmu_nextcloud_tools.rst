thegmu\_nextcloud\_tools package
================================

git\_sync\_data module
----------------------------------------------

.. automodule:: thegmu_nextcloud_tools.git_sync_data
    :members:
    :undoc-members:
    :show-inheritance:

script module
-------------------------------------

.. automodule:: thegmu_nextcloud_tools.script
    :members:
    :undoc-members:
    :show-inheritance:

thegmu\_davfs module
--------------------------------------------

.. automodule:: thegmu_nextcloud_tools.thegmu_davfs
    :members:
    :undoc-members:
    :show-inheritance:

thegmu\_log module
------------------------------------------

.. automodule:: thegmu_nextcloud_tools.thegmu_log
    :members:
    :undoc-members:
    :show-inheritance:

thegmu\_nextcloud\_tools.thegmu\_nextcloud\_git\_sync module
------------------------------------------------------------

.. automodule:: thegmu_nextcloud_tools.thegmu_nextcloud_git_sync
    :members:
    :undoc-members:
    :show-inheritance:

