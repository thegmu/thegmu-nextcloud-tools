Tools
=====

.. _tools_git_sync:

GIT Sync - mirror a GIT repo as an nextCloud folder
---------------------------------------------------

Here at The GMU, https://www.thegmu.com, we want to be able to share our GIT document folder with our Dropbox-like file sharing software, nextCloud, https://nextcloud.org. Our company is just two people, the two owners. We are both software developers and are comfortable using GIT for document control. Our intitial requirement is to be able to share our documents under GIT version control using the same features provided by nextCloud. For example:

#. We want to share files by email address with our accountant and other users who are not registered with our nextCloud server.
#. We also want to be able to have user created groups for sharing with groups.
#. We also want to have the ability to create temporary URL links for one time use.

All the features listed are available with nextCloud but missing from Github or Bitbucket where repositories are either public or private and no finer grain sharing control is available.

The design is simple enough in concept where the git repos are mirrored as read-only on the nextCloud host. The GIT sync tool updates nextCloud every minute with any new files or changes to the GIT repository.

The implementation was much more involved. For example one cannot just copy files from a GIT folder to the corresponding nextCloud folder because the file needs to be registered with the nextCloud database. This registration requires uploading the file from the server to the nextCloud software in the exact same way any client does.

**GIT Sync Requirements**:

#. Mirror a GIT repo as read-only to a configured nextCloud user account.
#. All accidental uploads to the GIT folder are to be reverted to the GIT version of the file.
#. A special transfer directory, 'Transfer', will allow uploads.
#. Files transfered to the transfer directory from say a phone are to be moved to an appropropriate GIT folder in a timely fashion.
#. Files transfered are purged weekly.
#. Daily email report of all files processed for the day.
#. Daily email of all errors processing files included in the daily email report.
#. Configuration file management. Adding a repo per user can be accomplished by creating a new configuration file.
#. Include and exclude directory options. The GIT repo directories can be filtered by using either an include or exclude list.

.. note::

  Linux shell commands are frequently executed instead of using built-in Python compatible functions for the following reasons.

  #. **nextCloud files**: sudo is required for nextCloud files that are owned by one account. Since we are using the Apache web server that account is 'www-data'. 
  #. **GIT files**:  sudo is required for GIT files that are owned by one account on our server, 'root'.
  #. **davfs**: sudo is required for the account that mounts the davfs folder used to upload to nextCloud.
