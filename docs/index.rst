.. module:: thegmu-nextcloud-tools
   
thegmu-nextcloud-tools - Mirror a git repo within nextCloud & more.
===================================================================

:mod:`thegmu-nextcloud-tools` is a collection of Python tools for providing additional features to nextCloud (https://nextcloud.com). The nextCloud software is a _Dropbox_ like file sharing service that you host yourself. 
    
- Documentation: http://the-gmu-nextcloud-tools.readthedocs.org/
- Source Code: https://bitbucket.org/thegmu/thegmu-nextcloud-tools
- Download: http://pypi.python.org/pypi/thegmu-nextcloud-tools
- Mail: mybrid@thegmu.com

Please feel free to ask questions, report installation problems, bugs or any other issues to the email address provided above.

.. note::

 Version 0.2.x is an alpha release.  Overall requirements:
 
 #. Linux, nextCloud supported
 #. Python 3.6+
 #. nextCloud 10.1.0
 #. GIT repos mirrored on the nextCloud host.
   
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   tools
   intro
   policy
   configure
   cron

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
