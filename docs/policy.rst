Policy
======

.. _policy_questions:

Policy Questions
----------------

All the following questions need to be answered for fully configuring the application. System administrators are expected to understand how to create cron scripts from  the sample cron scripts provided for the policies.



.. _policy_git_directory:

GIT Policy
----------

* Q: How many GIT repo directories are to be mirrored?

* Q: Which Linux account will host the GIT repo?

* Q: Which GIT directories for a repo are to be included if any?

* Q: Which GIT directories for a repo are to be excluded if any? 

* Q: Which GIT repo, branch, or tag version for a repo is to be mirrored?

.. note::
   The configuration file includes a simple "git pull" command. This command can be modified with version configuration if needed. The GIT repo directory has files that are checked out. The branch that is currently checked out can be sync'ed without setting the branch in the configuration file.

* Q: Will there be large files copied to the GIT directory for mirroring? 

.. note::
   The mirror of the git directoy will mirror all files and thus files not under GIT control can be copied to the git directory. For example, you may have large video and audio files to mirrored in a read-only fashion identical to the GIT controled files. 

* Q: Who can push to the GIT repo? 

.. _policy_nextcloud_directory:

nextCloud Policy
----------------
* Q: Which nextCloud account will be mirrored too?
* Q: Will the nextCloud account share to a group?


.. _policy_thegmu_configuration_file:

Configuration File Policy
-------------------------

* Q: Who can edit the configuration files? If users are adding or editing configuration files then we do not recommend using `/etc/thegmu` directory for configuration files.
* Q: Where should the configuration files reside? We recommend `/etc/thegmu` if only the system administrator is editing the files.
* Q: Where should the application log files go? Each configuration file needs a distinct set of log files. Log files can go in the same directory configured with different file names or log files can go in separate directories with the same name. 

.. _policy_git_synchronization:

GIT Sychronization Policy
-------------------------

* Q: How often should the GIT files be synced with nextCloud in cron?

.. note::

    We recommend every minute. The application supports syncing every minute. Longer times like very five mintues may experience only some files copied per sync. If a file is copied the entire file is always copied, but on all files may be copied. This is due to a time-out of thirty seconds for files copied. This means only some files copies an happen every minute until all files of some new commit are synced. The DavFS performance is highly variable depending on the load of the nextCloud server. The DavFS cache needs to be flushed and this can take longer than the reported copy time by the Linux copy command. We found that limiting the DavFS copy load to at most thirty seconds creates manageable DavFS flush performance and a low impact on the nextCloud server.

.. _policy_report_mail:

Report Email Policy
-------------------

You can safely ignore this section if you do not have a policy to email daily reports.


* Q: What GIT repos need reporting?
* Q: What email accounts need the report email?
* Q: How often should the report be run? 

.. note::
   The report is always daily. However, the changes throughout the day can be picked up by running the report hourly where the report will always contain all the files processed for the day.

   
.. _policy_continue:

On To Configuration
-------------------

Once you have your policy plan in place then the next step is to create configuration file. 

