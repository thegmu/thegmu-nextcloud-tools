CRON
====

.. _cron_synchronization:

CRON Synchronization
--------------------

To ``root`` or not to ``root``. The synchronization uses sudo to mount DavFS as well as run the ``git pull`` command. A properly configured user account can be used as long as these commands execute.

* Backup: If using the ``root`` account then the crontab file ``/etc/crontab`` can be updated. Always make a backup, i.e.::

    cp /etc/crontab /etc/crontab.bck

If using the account crontab then backup as::

    crontab -l > crontab.txt.bck
    crontab -l > crontab.txtk


* Environment: There are two environment variables to be set in the crontab configuration, one specifying the configuration file directory and the second specifying the files::
    
    # /etc/crontab, ~/crontab.txt
    # export THEGMU_CONFIG_DIR="/etc/thegmu"
    # export THEGMU_OCGS_CONFIG_FILES="thegmu_nextcloud_git_sync.nextcloudhost.yaml thegmu_nextcloud_git_sync.youraccount.yaml"
    
* Script: A ``bash`` script was installed to synchronize the list of configuration files::
    
    # /etc/crontab    
    # m h dom mon dow user	command
    *  *    * * *   root   test -x /usr/local/bin/thegmu_nextcloud_git_sync.sh && /usr/local/bin/thegmu_nextcloud_git_sync.sh

    # ~/crontab.txt
    # m h dom mon dow 	command
    *  *    * * *   /usr/local/bin/thegmu_nextcloud_git_sync.sh

.. _cron_report:

CRON Report
-----------

The application script provides a report of all files processed for the current day or previous day. The report is printed to console::

    thegmu-nextcloud-git-sync.py -y thegmu_nextcloud_git_sync.yourhost.yaml
    Processed:
    Documents/foo.txt,20190331:055015.52,COPY
    Documents/bar.txt,20190331:055015.52,COPY

The reported file listing is relative to the nextCloud directories as specified in the nextCloud interface. They are reported relative to nextCloud because of deletions. If accidental upload of files to the nextCloud/GIT directory occurs then the files will be deleted and reported as such.

The report ``cron`` job then just needs to redirect the output to a file and mail that file to a set of recipients.


.. note:: The package does not ship with any example report mail cron script because the variation is more than what is constant.

Below is an example we use currently. Feel free to copy this and update. We run this script daily as the ``root`` user by copying the script to the Linux daily cron folder::
    /etc/cron.daily/

Example report script::
    
    2019-04-02 07:35:46 % cat /etc/cron.daily/gitcloudsync_gmucorp_daily_report 
    #!/bin/bash
    
    SHELL=/bin/bash
    PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

    MAILTO="greggy@thegmu.com mybrid@thegmu.com"
    #MAILTO="mybrid@thegmu.com"
    MAILFROM="greggy@thegmu.com"
    MAILDAY=`date +"%m/%d/%Y"`
    MAILSUBJECT="[GMU] $MAILDAY NextCloud 'gmucorp' files processed yesterday."
    
    GITCLOUDSYNC_SCRIPT="/usr/local/bin/thegmu_nextcloud_git_sync.py"
    GITCLOUDSYNC_YAML="/etc/thgmu/thegmu_nextcloud_git_sync.gmucorp.yaml"
    GITCLOUDSYNC_MAIL_FILE="/tmp/gitcloudsync_mail.txt"
    
    GITCLOUDSYNC_REPORT="$GITCLOUDSYNC_SCRIPT file not found."

    if [ -f $GITCLOUDSYNC_SCRIPT ]; then
        echo "$GITCLOUDSYNC_SCRIPT running..."
        python3 $GITCLOUDSYNC_SCRIPT --yesterday_report $GITCLOUDSYNC_YAML > $GITCLOUDSYNC_MAIL_FILE 2>&1
    else
        echo $GITCLOUDSYNC_REPORT > $GITCLOUDSYNC_MAIL_FILE
    fi

.. _configuration_complete:

Configuration Complete
----------------------

Configuration is now complete.
